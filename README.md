# Starter Web L1 Info


## Description
Ce dépôt gitlab contient les fichiers de base pour la création d'un site web html/css/javascript.
La structure et les fichiers proposés pourront être utilisés par les étudiants de Licence 1 Informatique de l'Université de La Rochelle. 

## Installation
Pour utiliser ce Starter, vous devez le cloner : dans un terminal (par exemple le terminal accessible par *Visual Studio Code*), tapez la commande :

```git clone https://gitlab.univ-lr.fr/prodrigu/starter-web-l1-info.git TP2```

Cette commande crée une copie des fichiers du starter et les stocke dans un dossier ```TP2``` situé dans le dossier courant.
Vous pouvez démarrer votre éditeur (Visual Studio Code par exemple), et avec l'éditeur, vous devez ouvrir le **dossier** TP2. 


## Project status
Il s'agit de la version 1 de ce starter.
Elle servira d'introduction à la pratique de Git
